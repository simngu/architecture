@startuml

' hide the spot
hide circle

' avoid problems with angled crows feet
skinparam linetype ortho

' Remove shadows
skinparam shadowing false

skinparam Default {
    FontName Arial
    FontColor #133552  
    FontSize 20
}

skinparam class {
    BorderColor #133552
    BackgroundColor #cce4e8
 }

skinparam Arrow {
    Color #133552
}
 
entity Person {
    *Id : uuid
    --
    ExternalId : uuid
    FirstName : string
    LastName : string
    MainEmail : string
    MainPhone : string
    MainBankAccountNumber : string
    MainBankAccountIBAN : string
    BirthDate : date-time
    AffiliateDepartment : uuid <<FK>>
    Seniority : date-time
    MainAddressStreet1 : string
    MainAddressStreet2 : string
    MainAddressStreet3 : string
    MainAddressZip : string
    MainAddressCity : string
    MainAddressCountryCode : string
    NearestLeader : uuid <<FK>>
    Hidden : boolean
    Active : boolean
    CreatedDate : date-time
    LastModifiedDate : date-time
}

entity Phone {
    *Id : uuid
    --
    Value : string
    Type : enum
    OwnerID : uuid <<FK>>
    CountryPrefix : string
    CreatedDate : date-time
    LastModifiedDate : date-time
}

entity Email {
    *Id : uuid
    --
    Value : string
    Type : enum
    OwnerId : uuid <<FK>>
    CreatedDate : date-time
    LastModifiedDate : date-time
}

entity Address {
    *Id : uuid
    --
    Street1 : string
    Street2 : string
    Street3 : string
    Zip : string
    City : string
    CountryCode : uuid <<FK>>
    Type : enum
    OwnerId : uuid <<FK>>
    CreatedDate : date-time
    LastModifiedDate : date-time
}

entity BankAccount {
    AccountNumber : string
    IBAN : string
    CountryCode : string
    Type : enum
    OwnerId : uuid <<FK>>
    CreatedDate : date-time
    LastModifiedDate : date-time
}

entity IdentityIdentifiers {
    *Id : uuid
    --
    Value : string
    Type : enum
    OwnerId : uuid <<FK>>
    CountryCode : uuid <<FK>>
    CreatedDate : date-time
    LastModifiedDate : date-time
}

entity OrganizationalUnit {
    *Id : uuid
    --
    ExternalId : uuid
    TypeId : enum
    Hidden : boolean
    Active : boolean
    Name : string
    DepartmentCode : string
    ManagerId : uuid <<FK>>
    ParentId : uuid <<FK>>
    OrganizationNumber : string
    CompanyNumber : string
    NACE : string
    SystemCountry : uuid <<FK>>
    SystemCurrency : enum
    CreatedDate : date-time
    LastModifiedDate : date-time
  }

entity CountryCode {
    *Id : uuid
    --
    Aplha2Code: string
    Name: string 
    CreatedDate : date-time
    LastModifiedDate : date-time
}

entity Property {
    *Id : uuid
    --
    Value : string
    PropertyType : uuid <<FK>>
    OwnerId : uuid <<FK>>
    CreatedDate : date-time
    LastModifiedDate : date-time
}

entity PropertyType {
    *Id : uuid
    --
    LegalValues : list
    UserEditable : boolean
    OwnerType : enum
    ValueType : enum
    CreatedDate : date-time
    LastModifiedDate : date-time
}

entity Employee {
    *Id : uuid
    --
    PersonId : uuid <<FK>>
    OrganizationalUnit : uuid <<FK>>
    Number : string
    CreatedDate : date-time
    LastModifiedDate : date-time
}

entity Employment {
    *Id : uuid
    --
    Employee : uuid <<FK>>
    OrganizationalUnit : uuid <<FK>>
    StartDate : date-time
    EndDate : date-time
    EndReason : uuid <<FK>>
    Position : uuid <<FK>>
    Category : uuid <<FK>>
    AssignmentUnit : enum
    AssignmentValue : decimal
    Active : boolean
    CreatedDate : date-time
    LastModifiedDate : date-time
}

entity Position {
    *Id : uuid
    --
    NationalCode : string
    CountryCode : uuid <<FK>>
    Name : string
    Number : string
    Description : string
    Active : boolean
    CreatedDate : date-time
    LastModifiedDate : date-time
}

entity EndReason {
    *Id : uuid
    --
    Name : string
    Code : string
    Description : string
    Active : boolean
    IsSystem : boolean
    Reason : enum
    CountryCode : uuid <<FK>>
    CreatedDate : date-time
    LastModifiedDate : date-time
}

entity Category {
    *Id : uuid
    --
    Number : string
    Name : string
    Description : string
    HoursPerUnit : decimal
    HoursUnit : enum
    CountInSickStatistics : boolean
    CountInFTE : boolean
    EndDateIsMandatory : boolean
    HoursPerUnitIsMandatory : boolean
    Active : boolean
    CreatedDate : date-time
    LastModifiedDate : date-time
}


Person ||..o{ Person: NearestLeader
Person }o..|| OrganizationalUnit: AffiliateDepartment
OrganizationalUnit }o..|| Person: Manager
OrganizationalUnit ||..o{ OrganizationalUnit: Parent
OrganizationalUnit }o..|| CountryCode: SystemCountry
Phone }o..|| Person
Email }o..|| Person
Address }o..|| CountryCode
Address  }o..|| Person
Address  }o..|| OrganizationalUnit
BankAccount }o..|| Person
BankAccount ||..o{ CountryCode
IdentityIdentifiers ||..o{ CountryCode
IdentityIdentifiers }o..|| Person
Property }o..|| Person
Property }o..|| PropertyType
Employee }o..|| Person
Employee }o..|| OrganizationalUnit: LegalUnit
Employment }o..|| Employee
Employment }o..|| OrganizationalUnit: Department
Employment ||..o| EndReason
Employment ||..|| Position
Employment }o..|| Category
EndReason }o..|| CountryCode
Position }o..|| CountryCode
@endum